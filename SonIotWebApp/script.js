var app = angular.module("SonIoT", ['ui.slider', 'ui.bootstrap', 'ngGrid']);

// singletons
app.factory("shared", function ($http) {
    (function () { console.log('service init'); })();

    var actx = new (window.AudioContext || window.webkitAudioContext)();

    // to play silence / None sample
    var emptyBuf = actx.createBuffer(1, 1, 44100);

    var defaultSamples = ['dr_kick.wav', 'dr_snare.wav', 'meow.wav', 'scratch_1.wav', 'scratch_2.wav', 'coin.wav', 'vo_oh.mp3', 'vo_huh.mp3', 'vo_hah.mp3', 'Snap.mp3'];
    var samples = {};

    function uploadSample(url) {
        $http({
            method: 'GET',
            url: url,
            responseType: 'arraybuffer'
        }).success(function (data) {
            var fileName = url.substring(url.lastIndexOf('/') + 1);
            console.log('successfully uploaded sample: ' + fileName);

            actx.decodeAudioData(data, function (buf) {
                samples[fileName] = buf;
            })
        }).error(function (data, status) {
            console.log('error uploading file: ' + status);
        })
    }

    // load default samples
    for (var i = 0; i < defaultSamples.length; i++) {
        var path = 'resources/samples/' + defaultSamples[i];
        uploadSample(path);
    }

    return {
        masterLevel: .5,
        curNumVoices: 0,
        maxNumVoices: 0, // this just records the max num reached
        numVoicesLimit: 10,

        scaleSelected: 'Chromatic',

        scales: {
            'Chromatic': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            'Major': [0, 2, 4, 5, 7, 9, 11],
            'Mixolydian': [0, 2, 4, 5, 7, 9, 10],
            'Dorian': [0, 2, 3, 5, 7, 9, 10],
            'Minor': [0, 2, 3, 5, 7, 8, 10],
            'Maj Penta': [0, 2, 4, 7, 9],
            'Min Penta': [0, 3, 5, 7, 10]
        },

        // {fileName: buffer}
        samples: samples,

        // temp
        defaultSamples: defaultSamples,

        emptyBuf: emptyBuf,

        getAudioContext: function () {
            return actx;
        },

        applyLogCurve: function (input, steepness) {
            steepness = steepness <= 1 ? 1.000001 : steepness;
            return (Math.log(input * (steepness - 1.) + 1.)) / (Math.log(steepness));
        },

        applyExpCurve: function (input, steepness) {
            steepness = steepness <= 1 ? 1.000001 : steepness;
            return (Math.exp(input * Math.log(steepness)) - 1.) / (steepness - 1.);
        },

        uploadSample: uploadSample
    };
});

app.controller("MainController", function ($scope, $http, shared, Synth, $modal) {
    $scope.shared = shared;

    $scope.init = function () {
        console.log('controller init');

//        loadClip(); // Load single audio clip.

        // TODO: change the view ui.range(n) to something better

        var defaultPitches = [.15, .25, .35, .45, .55, .65, .75, .85];

        for (var i = 0; i < $scope.ui.numKwInputBox; i++) {
            console.log('creating a synth instance: ' + i);
            $scope.synths[i] = new Synth();
            $scope.synths[i].pitchNrm = defaultPitches[i];
        }

//        angular.forEach(shared.scales, function (value, key) {
//
//        });

    };

    //===========================
    // offline JSON data
    // TODO: probably we should move the data into the shared service
    $scope.data = {
        kwPlaceholder: 'search',
        sources: ['Manual', 'Latitude', 'Longitude', 'Sentiment', 'Sent Pos', 'Sent Neg', 'Categories'],
        categories: ['eei_off_topic', 'warning', 'eei_incident', 'eei_infrastructure', 'eei_operations', 'eei_transportation'],

        selected: 'nysandy',

        loadData: function () {
            var datafile = 'resources/nysandy.noduplicate.json';

            if ($scope.data.source == 'atlsnow') {
                datafile = 'resources/atlsnow_geo_time.json';
            }

            $.get(datafile, function (jsonCol) {
                data = jsonCol.split('\n');
                dataLoaded = true;

                console.log('done loading data json');

                $scope.synthGlobal.createMiscSynth($scope.ui.numKwInputBox);
                $scope.clock.start(parseInt(stats['timestamp']['min']));
            }, 'text');
        },

        loadStats: function () {
            var statsfile = 'resources/nysandy_stats.json';

            if ($scope.data.source == 'atlsnow') {
                statsfile = 'resources/atlsnow_stats.json';
            }

            $.getJSON(statsfile, function (jsonData) {
                stats = jsonData;

                latMax = stats['location']['lat']['max'];
                latMin = stats['location']['lat']['min'];
                lngMax = stats['location']['lng']['max'];
                lngMin = stats['location']['lng']['min'];
                latMid = stats['location']['lat']['mid'];
                lngMid = stats['location']['lng']['mid'];

                latRange = latMax - latMin;
                lngRange = lngMax - lngMin;

                console.log('done loading stats json');

                if ($scope.data.source == 'atlsnow') {
                    $scope.map.init(33.7758, -84.3947);
                }
                else {
                    $scope.map.init(latMid, lngMid);
                }

                $("#time-indicator").progressbar({max: parseInt(stats['timestamp']['range'])});

                $scope.drawClock();
            });
        },

        loadKeywords: function () {
            var kwFile = 'resources/nysandy_keywords.txt';

            if ($scope.data.source == 'atlsnow') {
                kwFile = 'resources/atlsnow_keywords.txt';
            }

            $.get(kwFile, function (kwLines) {
                $scope.data.kwList = kwLines.split('\n');
                console.log('done loading keyword list');
            }, 'text');
        }
    };

    var data;
    var dataLoaded = false;
    var stats;

    // TODO: replace w/ angular's service?
    var latMin, latMax, lngMin, lngMax, latMid, lngMid;
    var latRange, lngRange;

    var bufferClip;

//    function loadClip() {
//        var request = new XMLHttpRequest();
//        request.open("GET", 'resources/Snap.mp3', true); // Path to Audio File
//        request.responseType = "arraybuffer"; // Read as Binary Data
//
//        request.onload = function() {
//            actx.decodeAudioData(request.response, function(buffer) {
//                bufferClip = buffer;
//            });
//        };
//
//        request.send();
//    }

    // Play Function for loaded soundfiles
//    function playClip(buffer){
//        var source = actx.createBufferSource();
//        source.buffer = buffer;
//        var amp = actx.createGain();
//        amp.gain.value = 0.2;
//        source.connect(amp);
//        amp.connect(actx.destination);
//        source.start(actx.currentTime);
//    }

    $scope.ui = {
        keywords: [],
        indicators: [],
        numKwInputBox: 8,
        colors: ["#FF0000","#FF8000","#FFFF00", "#00FF00", "#12FF12", "#0000FF", "#8000FF", "#FF00C0", "#FFFFFF"],

        range: function (n) {
            $scope.ui.numKwInputBox = n;
            return new Array(n);
        },

        paramSlider: {
            min: 0,
            max: 1,
            step: 0.01,
            range: 'min'
        }
    };


    //===========================
    // audio stuff
    var actx = shared.getAudioContext();

    // "It is recommended that authors do not specify buffSize (set to 0)"
    // re: this processingNode is solely used as a precise timer, so it should be as small as possible
    var pnode = actx.createScriptProcessor(256, 1, 1);

    // this holds multiple synth instances, each w/ unique param settings
    $scope.synths = [];

    $scope.synthGlobal = {
        wfPreset: ['sine', 'square', 'sawtooth', 'triangle'],
        scaleList: ['Chromatic', 'Major', 'Mixolydian', 'Dorian', 'Minor', 'Maj Penta', 'Min Penta'],
        samples: ['None', 'Upload'],
        bufList: [],

        setScale: function (s) {
            shared.scaleSelected = s;
        },

        createMiscSynth: function (id) {
            // for non-matched voice
            $scope.synths[id] = new Synth();
            $scope.synths[id].soundSrcType = 'Sample';
            $scope.synths[id].sample = 'Snap.mp3';
            $scope.synths[id].buffer = shared.samples['Snap.mp3'];
            $scope.synths[id].pitchNrm = .2;
            $scope.synths[id].fmSwitch = false;
        }
    };

    $scope.clock = {
        hz: 1,
        id: 0,
        timeIncr: 1000,
        date: '',

        start: function (curTime) {
            console.log('starting clock');

            pnode.connect(actx.destination);
            var prevAudTime = 0;

            var minTime = parseInt(stats['timestamp']['min']);

            // get the initial timestamp val
            var jsonData = $.parseJSON(data[$scope.clock.id]);
            var timestamp = parseInt(jsonData['timestamp']);

            while (timestamp < curTime) {
                $scope.clock.id++;
                jsonData = $.parseJSON(data[$scope.clock.id]);
                timestamp = parseInt(jsonData['timestamp']);
            }

            pnode.onaudioprocess = function (e) {
                if ((actx.currentTime - prevAudTime) >= (1/$scope.clock.hz)) {
//                    clearPointsFromMap();

                    // play when the tweet is in this time frame
                    while ((timestamp >= curTime) && (timestamp < (curTime + $scope.clock.timeIncr))) {
                        playSoundFromData($scope.clock.id);

                        // Are we updating visuals on the same clock as audio?
                        // Re: yes, so the audio & visual sync well. But maybe it's an overkill.
                        // Right now the clock rate is about .006ms or 166Hz.
                        // Although it's not redrawing stuff on every tick, but only when needed,
                        // we could use a lower-rate (maybe the standard JS) clock for visuals.
//                        addPointsToMap($scope.clock.id);

                        $scope.clock.id++;

                        // get the next timestamp val
                        jsonData = $.parseJSON(data[$scope.clock.id]);
                        timestamp = parseInt(jsonData['timestamp']);
                    }


                    // TODO: progressbar not updating / drawing smoothly?
                    $("#time-indicator").progressbar("value", Math.round(curTime-minTime));

                    curTime += $scope.clock.timeIncr;
                    prevAudTime = actx.currentTime;

                    $scope.$apply($scope.clock.showDate(curTime));
                }
            };
        },

        // TODO: improve this wacky search algorithm
        updatePos: function (newValNormalized) {
            var dataLen = stats['length'];

            var newCurId = Math.round(newValNormalized * (dataLen-1));
            var prevId, destId;

            var newCurTime = Math.round(newValNormalized * stats['timestamp']['range'] + stats['timestamp']['min']);

            var jsonData = $.parseJSON(data[newCurId]);
            var timestamp = parseInt(jsonData['timestamp']);

            if (timestamp < newCurTime) {
                destId = dataLen - 1;
            } else if (timestamp > (newCurTime + $scope.clock.timeIncr)) {
                destId = 0;
            }

            prevId = destId;

            while ((timestamp < newCurTime) || (timestamp > (newCurTime + $scope.clock.timeIncr))) {
                while (timestamp < newCurTime) {
                    increment();
                    if (Math.abs(destId - newCurId) <= 1) {
                        break;
                    }
                }

                destId = prevId;

                while (timestamp > (newCurTime + $scope.clock.timeIncr)) {
                    increment();
                    if (Math.abs(destId - newCurId) <= 1) {
                        break;
                    }
                }

                destId = prevId;

                if (Math.abs(destId - newCurId) <= 1) {
                    break;
                }
            }

            $scope.clock.id = newCurId;
            $scope.clock.start(newCurTime);

            function increment() {
                newCurId += Math.round((destId - newCurId) / 2);
                jsonData = $.parseJSON(data[newCurId]);
                timestamp = parseInt(jsonData['timestamp']);
                prevId = newCurId;
            }
        },

        showDate: function (timeStamp) {
            var date = new Date(timeStamp);
            var year = date.getFullYear();
            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var month = months[date.getMonth()];
            var day = ("0" + date.getDate()).slice(-2);
            var hour = ("0" + date.getHours()).slice(-2);
            var min = ("0" + date.getMinutes()).slice(-2);
            var sec = ("0" + date.getSeconds()).slice(-2);
            $scope.clock.date = month+'. '+day+', '+year+' '+hour+':'+min+':'+sec;
        },

        setSpeedFromSlider: function () {
            var normalizedVal = $("#speed-slider").slider("value") / 1000;
            $scope.clock.hz = (normalizedVal * 100) + 1;

            var adj = 10000;

            if ($scope.data.source == 'atlsnow') {
                adj *= 10;
            }

            $scope.clock.timeIncr = Math.round((normalizedVal * adj)) + 1000;
        }
    };

    function playSoundFromData(index) {
        var srcId = 0;

        var jsonData = $.parseJSON(data[index]);
        var lngNrm, latNrm, kwArray;

        if ($scope.data.source == 'nysandy') {
            lngNrm = (jsonData['location']['lng'] - lngMin) / lngRange;
            latNrm = (jsonData['location']['lat'] - latMin) / latRange;
            kwArray = jsonData['keywords'];
        }
        else {
            lngNrm = (jsonData['coordinates']['coordinates'][0] - lngMin) / lngRange;
            latNrm = (jsonData['coordinates']['coordinates'][1] - latMin) / latRange;
            var text = jsonData['text'];

            kwArray = text.split(' ');
        }

        // TODO: this is only temporary
        var scope = angular.element($("body")).scope();
        for (var j = 0; j < $scope.ui.numKwInputBox; j++) {
            scope.$apply(function () {
                scope.ui.indicators[j] = '';
            });
        }

        var matched = false;
        var matchedKW = ''; // The matching keyword: keep it for the log.

        for (var i = 0; i < kwArray.length; i++) {
            // the index num of matched keyword. -1 if there is no matches
            // TODO: check if index search continues after the first match
            var kwId = $scope.ui.keywords.indexOf(kwArray[i]);

            if (kwId >= 0) {
                var sent = jsonData['sent'];
                var sentNrm = (sent + 1) / 2;
                var sentPos = (sent >= 0) ? sent : 0;
                var sentNeg = (sent < 0) ? -(sent) : 0;

                var cat = jsonData['categories'];
                for (var j = 0; j < cat.length; j++) {
                    var catNrm = $scope.data.categories.indexOf(cat[j])/($scope.data.categories.length-1);
                }

                // Figure out the matched keyword.    
                matchedKW = $scope.ui.keywords[kwId];
//                console.log(matchedKW);

                //=============================================
                // big ugly remapping section
                // (not sure how to contain in a reusable function,
                // the destination param needs to be passed in as reference, not value)
                var paramCol = ['', latNrm, lngNrm, sentNrm, sentPos, sentNeg, catNrm];

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].pitchSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].pitchNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].levelSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].levelNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].xSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].xNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].ySrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].yNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].zSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].zNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].fmRatioSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].fmRatioNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].fmIndexSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].fmIndexNrm = paramCol[srcId];
                }

                srcId = $scope.data.sources.indexOf($scope.synths[kwId].lpfSrc);
                if ((srcId > 0) && (srcId < paramCol.length)) {
                    $scope.synths[kwId].lpfNrm = paramCol[srcId];
                }

                //=============================================
                $scope.synths[kwId].play();


                // TODO: also change / remove this later
                scope.$apply(function () {
                    scope.ui.indicators[kwId] = '!';
                });

//                addPointsToMap($scope.clock.id, kwId);
                $scope.map.queue.push([$scope.clock.id, kwId, $scope.map.pLife]);

                matched = true;
                // Change text of matched box
                $('#message-matches').val(jsonData['text']);
                scope.$apply(function () {
                    $scope.logData.unshift({
                        time: $scope.clock.date, 
                        match: matchedKW, 
                        name: jsonData['user']['screen_name'], 
                        sentiment: jsonData['sent'], 
                        tweet: jsonData['text']});
                });
            }
        }

        // Goes through the whole for loop and if still no matching keyword, then:
        if (!matched) {
//            playClip(bufferClip);
            $scope.synths[$scope.ui.numKwInputBox].xNrm = lngNrm;
            $scope.synths[$scope.ui.numKwInputBox].yNrm = latNrm;
            $scope.synths[$scope.ui.numKwInputBox].play();

//            addPointsToMap($scope.clock.id, 8);
            $scope.map.queue.push([$scope.clock.id, $scope.ui.numKwInputBox, $scope.map.pLife]);
        }

//        console.log(shared.curNumVoices);
        if (shared.curNumVoices > shared.numVoicesLimit) {
            console.log('voice overloading!: ' + shared.curNumVoices);
        }
    }

    $scope.drawClock = function () {
        setInterval(function () {
            $scope.map.clearPoints();

            for (var i = 0; i < $scope.map.queue.length; i++) {
                $scope.map.drawPoint($scope.map.queue[i]);
                $scope.map.queue[i][2]--;

                if ($scope.map.queue[i][2] == 0) {
                    $scope.map.queue.splice(i, 1);
                }
            }

//            $scope.map.queue.length = 0; // empty the $scope.map.queue array
        }, 33);
    };

    //===========================
    // map stuff
    var map, vectorLayer;
    var fromProjection, toProjection;
    var layer_style;
    var radius = 4;

    $scope.map = {
        pLife: 20,
        queue: [],

        init: function (lat, lon) {
            console.log('init map');

            map = new OpenLayers.Map("map");
            var mapnik = new OpenLayers.Layer.OSM();
            fromProjection = new OpenLayers.Projection("EPSG:4326"); // Transform from WGS 1984
            toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
            var position = new OpenLayers.LonLat(lon, lat).transform(fromProjection, toProjection);
            var zoom = 8;

            map.addLayer(mapnik);
            map.setCenter(position, zoom);

            layer_style = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style['default']);
            layer_style.fillOpacity = 0.2;
            layer_style.graphicOpacity = 1;

            var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
            renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

            vectorLayer = new OpenLayers.Layer.Vector("Vector Layer", {
                style: layer_style,
                renderers: renderer
            });

            map.addLayer(vectorLayer);

            map.events.register('zoomend', map, function () {
                radius = map.getZoom() * 3.0 - 20;
                radius = (radius < 1) ? 1 : radius;
            })
        },

        drawPoint: function (pointParam) {
            var id = pointParam[0];
            var kwmatch = pointParam[1];
            var life = pointParam[2];

            var style_circle = OpenLayers.Util.extend({}, layer_style);
            //var colors = ["#FF0000","#FF8000","#FFFF00", "#00FF00", "#00FF00", "#0000FF", "#8000FF", "#FF00C0", "#FFFFFF"];
            style_circle.strokeColor = $scope.ui.colors[kwmatch];
            style_circle.fillColor = $scope.ui.colors[kwmatch];
            style_circle.strokeOpacity = "1";

            var opacity = shared.applyLogCurve((life / $scope.map.pLife), 5);
            style_circle.fillOpacity = opacity.toString();
            style_circle.graphicName = "circle";
            style_circle.strokeWidth = 1;

            var jsonData = $.parseJSON(data[id]);
            var lat, lon, kwArray;

            if ($scope.data.source == 'nysandy') {
                lon = (jsonData['location']['lng']);
                lat = (jsonData['location']['lat']);
                kwArray = jsonData['keywords'];
            }
            else {
                lon = (jsonData['coordinates']['coordinates'][0]);
                lat = (jsonData['coordinates']['coordinates'][1]);
                var text = jsonData['text'];
                kwArray = text.split(' ');
            }

            // TODO: get the offline keyword length stats?
            var minKwLen = 1;
            var maxKwLen = 15;
            var kwLen = kwArray.length;
            var size = (kwLen - minKwLen) / (maxKwLen - minKwLen);
            size = size < 0 ? 0 : size;
            size = size > 1 ? 1 : size;

            style_circle.pointRadius = radius * (size * 1.5 + 1);

            var point = new OpenLayers.Geometry.Point(lon, lat).transform(fromProjection, toProjection);
            var pointFeature = new OpenLayers.Feature.Vector(point, null, style_circle);

            vectorLayer.addFeatures([pointFeature]);
        },

        clearPoints: function () {
            vectorLayer.removeAllFeatures();
        }
    };


    //===========================
    // misc UI

    $scope.jqui = {};

    $(function () {
        $("#speed-slider").slider(
            {
                orientation: "vertical",
                min: 0,
                max: 1000,
                value: 15,
                range: "min",
                slide: $scope.clock.setSpeedFromSlider,
                change: $scope.clock.setSpeedFromSlider
            }
        );

        $("#seek-slider").slider(
            {
                orientation: "horizontal",
                min: 0,
                max: 1000,
                value: 0,
                change: updateCurTime
            }
        )
    });

    $(document).ready(function () {
        $("#time-indicator").progressbar();
    });

    function updateCurTime() {
        var newValNormalized = $('#seek-slider').slider('value') / 1000;
        $scope.clock.updatePos(newValNormalized);

        var newSliderVal = newValNormalized * stats['timestamp']['range'];
        $('#time-indicator').progressbar('value', newSliderVal);
    }

    $scope.openAudioMapper = function (id) {
        console.log('opening audio mapping window: ' + id);

        var modalInstance = $modal.open({
            templateUrl: 'audiomap.html',
            controller: 'AudioMapperController',
//            size: 'sm',
            scope: $scope,
            resolve: {
                id: function () { return id; }
            }
        });
    };

    $scope.showLog = function (id) {
        console.log('opening audio mapping window: ' + id);

        var modalInstance = $modal.open({
            templateUrl: 'showLog.html',
            windowClass: 'log-dialog',
            size: 'lg',
            scope: $scope,
            resolve: {
                id: function () { return id; }
            }
        });
    };

    $scope.dataFileSelection = function () {
        console.log('opening the data selection window');

        var modalinstance = $modal.open({
            templateUrl: 'seldata.html',
            controller: 'SelDataSrcController',
            backdrop: 'static',
            size: 'sm',
            scope: $scope
        })
    };

    $scope.logData = [{}];
    $scope.gridOptions = { 
        data: 'logData',
        columnDefs:[
        {field:'time', displayName: 'Time', width: "12%"},
        {field:'match', displayName: 'Match', width: "10%"},
        {field:'name', displayName: 'Screen Name', width: "13%"},
        {field:'sentiment', displayName: 'Sentiment', width: "7%"},
        {field:'tweet', displayName: 'Tweet', width: "58%"}] 
        };

    $scope.init();
});

app.controller('AudioMapperController', function ($scope, $modalInstance, shared, id) {
    // note: these scope objects are reinitialized when closing / reopening the view

    // use this to match to the right synth instance
    $scope.id = id;

    $scope.uiRemapper = {
        setPitchSrc: function (s) { $scope.synths[id].pitchSrc = s; },
        setLevelSrc: function (s) { $scope.synths[id].levelSrc = s; },

        setXSrc: function (s) { $scope.synths[id].xSrc = s; },
        setYSrc: function (s) { $scope.synths[id].ySrc = s; },
        setZSrc: function (s) { $scope.synths[id].zSrc = s; },

        setFmRatioSrc: function (s) { $scope.synths[id].fmRatioSrc = s; },
        setFmIndexSrc: function (s) { $scope.synths[id].fmIndexSrc = s; },

        setLpfSrc: function (s) { $scope.synths[id].lpfSrc = s; },
        setRmSrc: function (s) { $scope.synths[id].rmSrc = s; }
    };

    $scope.soundSource = {
        categories: ['Synth', 'Sample'],
        setType: function (s) { $scope.synths[id].soundSrcType = s; },

        setWaveform: function (s) { $scope.synths[id].waveform = s; },
        setSample: function (s) {
            $scope.synths[id].sample = s;
            $scope.synths[id].buffer = shared.samples[s];
        },

        setNone: function () {
            $scope.synths[id].sample = 'None';
            $scope.synths[id].buffer = shared.emptyBuf;
        },

        uploadSample: function () {

        }
    };
});

//app.controller('GlobalConfigsController', function ($scope, $modalInstance) {
//
//});

app.controller('SelDataSrcController', function ($scope, $modalInstance) {
    $scope.dataSource = 'Select Data Source';
    $scope.dataSourceList = ['NY Hurricane Sandy', 'Atlanta Snow Storm'];

    $scope.setDataSource = function (s) {
        $scope.dataSource = s;

        if (s == $scope.dataSourceList[0]) {
            $scope.data.source = 'nysandy';
        }
        else if (s == $scope.dataSourceList[1]) {
            $scope.data.source = 'atlsnow';
        }
    };

    $scope.ok = function () {
        if ($scope.dataSource == 'Select Data Source') {
            window.alert('Please select a data source first');
        }
        else {
            $scope.data.loadData();
            $scope.data.loadStats();
            $scope.data.loadKeywords();

            $modalInstance.close();
        }
    };
});