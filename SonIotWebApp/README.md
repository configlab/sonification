# Sonify Internet of Things

v1.0

## Installation

Get the following zip files, unarchive them, and put them in the resources/ folder. The json file should go to the resources/ folder while the samples should be in resources/samples/ folder.

https://dl.dropboxusercontent.com/u/21355298/nysandy.noduplicate.json.zip
https://dl.dropboxusercontent.com/u/21355298/samples.zip

## Usage

Terminal command:
cd <this directory>
python -m SimpleHTTPServer

In browser:
localhost:8000/index.html

* Currently, I'm using jQuery's ajax methods to load the json data, which requires resources to be on HTTP server. 

## Dependencies

For WebAudio, any browsers should work besides IE and Opera. 