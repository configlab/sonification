// Synth class
app.factory("Synth", function (shared) {
    var actx = shared.getAudioContext();

    function Synth() {
        var self = this;
        var src, ampEnv, ampGain, pan;
        var testSrc, testOsc, testFmOsc;

        // TODO: have separate pitch params for OSC and sampler
        this.pitchSrc = 'Manual';
        this.pitchNrm = .5;
        var nnMin = 48;
        var nnMax = 108;

        this.levelSrc = 'Manual';
        this.levelNrm = .5;

        this.xSrc = 'Longitude';
        this.ySrc = 'Latitude';
        this.zSrc = 'Manual';
        this.xNrm = .5;
        this.yNrm = .5;
        this.zNrm = .5;

        this.fmSwitch = true;
        this.fmRatioSrc = 'Manual';
        this.fmIndexSrc = 'Manual';
        var fmRatio = 1;
        var fmIndex = 0;
        this.fmRatioNrm = .5;
        this.fmIndexNrm = 0;

        this.lpfSwitch = false;
        this.lpfSrc = 'Manual';
        this.lpfNrm = 1;
        var lpfMin = 50;
        var lpfMax = 12000;

        this.rmSwitch = false;
        this.rmSrc = 'Manual';
        this.rmNrm = 0;
//        var rmFreq = 0;

        this.soundSrcType = 'Synth';
        this.waveform = 'sine';
        this.sample = 'None';
        this.buffer = shared.emptyBuf;


        // this reduces the overall volume when previewing the note
        // TODO: this may not work correctly across all voices
        this.duck = false;

        function mtof(nn) {
            return 440. * Math.pow(2, (nn - 69) / 12.);
        }

        function midiToRatio(nn) {
            return Math.pow(2, (nn-60) / 12);
        }

        function scaler(nrm, min, max) {
            return nrm * (max - min) + min;
        }

        function pitchQuantize(nn) {
            var scale = shared.scales[shared.scaleSelected];
            var pc = nn % 12;
            var oct = nn - pc;
            var idx = Math.floor(pc / 12. * scale.length);
            return oct + scale[idx];
        }

        this.preview = function () {
            var now = actx.currentTime;
            self.duck = true;

            var amp = actx.createGain();

            var nn = Math.round(scaler(this.pitchNrm, nnMin, nnMax));
            nn = pitchQuantize(nn);

            if (self.soundSrcType == 'Synth') {
                testSrc = actx.createOscillator();
                testSrc.type = self.waveform;
                testSrc.frequency.setValueAtTime(mtof(nn), now);

                amp.gain.setValueAtTime(.3 * shared.masterLevel, now);
            }
            else if (self.soundSrcType == 'Sample') {
                testSrc = actx.createBufferSource();
                testSrc.buffer = self.buffer;
                testSrc.playbackRate.setValueAtTime(midiToRatio(nn), now);

                amp.gain.setValueAtTime(.6 * shared.masterLevel, now);
            }

            testSrc.connect(amp);
            amp.connect(actx.destination);

            if ((self.fmSwitch) && (self.soundSrcType == 'Synth')) {
                testFmOsc = actx.createOscillator();
                var fmAmp = actx.createGain();
                testFmOsc.connect(fmAmp);
                fmAmp.connect(testSrc.frequency);

                fmRatio = scaler(self.fmRatioNrm, 1, 10);
                fmIndex = scaler(self.fmIndexNrm, 0, 4000);

                testFmOsc.frequency.setValueAtTime(mtof(nn) * fmRatio, now);
                fmAmp.gain.setValueAtTime(fmIndex, now);

                testFmOsc.start();
            }

            if (self.lpfSwitch) {
                var lpf = actx.createBiquadFilter();
                lpf.type = 0;

                var freq = shared.applyExpCurve(self.lpfNrm, 10);
                freq = scaler(freq, lpfMin, lpfMax);

                lpf.Q.setValueAtTime(10, 0);
                lpf.frequency.setValueAtTime(freq, 0);

                amp.disconnect();
                amp.connect(lpf);
                lpf.connect(actx.destination);
            }

            testSrc.start();
        };

        this.stopPreview = function () {
            self.duck = false;

            if (testSrc != undefined)
                testSrc.stop();

            if (self.fmSwitch && (testFmOsc != undefined)) {
                testFmOsc.stop();
            }
        };

        this.play = function () {
            var now = actx.currentTime;
            var dur = 0.1;

            ampGain = actx.createGain();
            pan = actx.createPanner();

            ampGain.connect(pan);
            pan.connect(actx.destination);

            // audio level to be modulated from outside
            var level = this.levelNrm * 2;

            // this is an internal level attenuation
            var gain = .5;

            var x = this.xNrm * 4 - 2;
            var y = this.yNrm * 4 - 2;
            pan.setPosition(x, y, -.5);

            var nn = Math.round(scaler(this.pitchNrm, nnMin, nnMax));
            nn = pitchQuantize(nn);

            if (self.soundSrcType == 'Synth') {
                src = actx.createOscillator();
                src.type = self.waveform;
                src.frequency.setValueAtTime(mtof(nn), now);

                ampEnv = actx.createGain();

                src.connect(ampEnv);
                ampEnv.connect(ampGain);

                ampEnv.gain.setValueAtTime(1, now);
                ampEnv.gain.linearRampToValueAtTime(0., now + dur);

                if (self.fmSwitch) { applyFm(); }

            }
            else if (self.soundSrcType == 'Sample') {
                src = actx.createBufferSource();
                src.buffer = self.buffer;
                src.playbackRate.setValueAtTime(midiToRatio(nn), now);

                gain *= 1.5;
                src.connect(ampGain);
            }

            if (self.duck) { gain *= .1; }
            ampGain.gain.setValueAtTime(level * gain * shared.masterLevel, now);

            if (self.lpfSwitch) { applyLpf(); }

            src.start(now);

            if (self.soundSrcType == 'Synth')
                src.stop(now + dur);

            var id = 0; // per voice

            shared.curNumVoices++;
            if (shared.curNumVoices > shared.maxNumVoices) {
                shared.maxNumVoices = shared.curNumVoices;
            }
            src.onended = function () { shared.curNumVoices--; };
        };

        this.dropVoice = function () {

        };

        function applyFm() {
            var now = actx.currentTime;
            var dur = .1; // TODO: hmm

            var fmOsc = actx.createOscillator();
            var fmAmp = actx.createGain();
            fmOsc.connect(fmAmp);
            fmAmp.connect(src.frequency);

            fmRatio = scaler(self.fmRatioNrm, 1, 10);
            fmIndex = scaler(self.fmIndexNrm, 0, 4000);

            var nn = Math.round(scaler(self.pitchNrm, nnMin, nnMax));
            nn = pitchQuantize(nn);

            fmOsc.frequency.setValueAtTime(mtof(nn) * fmRatio, now);
            fmAmp.gain.setValueAtTime(fmIndex, now);

            fmOsc.start(now);
            fmOsc.stop(now + dur);
        }

        function applyLpf() {
            var lpf = actx.createBiquadFilter();
            lpf.type = 0;

            var freq = shared.applyExpCurve(self.lpfNrm, 10);
            freq = scaler(freq, lpfMin, lpfMax);

            lpf.Q.setValueAtTime(10, 0);
            lpf.frequency.setValueAtTime(freq, 0);

            ampGain.disconnect();
            ampGain.connect(lpf);
            lpf.connect(pan);
        }

        function applyRm() {
//            var rmOsc = actx.createOscillator();
//            var rmAmp = actx.createGain();
//            rmOsc.connect(rmAmp);
//            rmAmp.connect(amp.gain);
        }
    }

    return Synth;
});
