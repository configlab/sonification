import json
import codecs
import re
from collections import Counter

fr = open('nysandy.annotated.json', 'r')
fw = codecs.open('keywords.txt', 'w', 'utf-8')

keywords = []

lines = fr.readlines()

for i in lines:
	json_obj = json.loads(i)
	for j in json_obj['keywords']:
		keywords.append(j)

# # remove duplicates by converting into a set
# kw_unique = set(keywords)
# kw_list = sorted(list(kw_unique))

kw_ordered = Counter(keywords).most_common()

for kw in kw_ordered:
	# exclude any words containing special & numeric characters
	if re.search('\W+|\d+|_+', kw[0]) is None:
		fw.write(kw[0] + '\n')

fr.close()
fw.close()