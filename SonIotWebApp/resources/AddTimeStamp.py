'''
Adds timestamp to the original json file. Also reorders the data points if the sequence is not in ascending order.

'''

import json
import time
import datetime

fr = open('atlsnow_geo.json', 'r')
fw = open('atlsnow_geo_time.json', 'w')

lines = fr.readlines()
fr_len = len(lines)
timestamp_list = [None] * fr_len
timestamp_createdAt = [None] * fr_len

for i, j in enumerate(lines):
	json_obj = json.loads(j)

	# Convert to Unix Timestamp (taking into account UTC offset)
	timestamp_createdAt[i] = json_obj['created_at']
	timestamp_list[i] = int(time.mktime(datetime.datetime.strptime(json_obj['created_at'],'%a %b %d %H:%M:%S +0000 %Y').timetuple()))
	offset = json_obj['user']['utc_offset']
	if offset != None:
		timestamp_list[i] += int(offset)

	# use millisec, since NYSandy data is doing so...
	timestamp_list[i] *= 1000

	# fw.write(json.dumps(json_obj))
	# fw.write('\n')

# the timestamp is not in ascending order, so get the indices of sorted list
reorderedIdx = [i for i, v in sorted(enumerate(timestamp_list), key=lambda x:x[1])]

for i in range(fr_len):
	json_obj = json.loads(lines[reorderedIdx[i]])
	json_obj['timestamp'] = timestamp_list[reorderedIdx[i]]

	fw.write(json.dumps(json_obj))
	fw.write('\n')

fr.close()
fw.close()