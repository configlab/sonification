# A parser for removing repeated tweets from our json data
# 
# To run:
#  1) Put this file in the same location as 'nysandy.annotated.json'
#  2) Open terminal, cd into the directory
#  3) Run python parser.py
#


import json

data=[]
with codecs.open('nysandy.annotated.json') as f:
	for line in f:
		data.append(json.loads(line))

id = 0 # initialize
ndata=[]

for i in range(len(data)):
	if data[i]['id'] != id :
		ndata.append(data[i])
	id = data[i]['id']
	print i

with open('test.json', "w") as myfile:
	myfile.write('\n'.join(json.dumps(ndata[i]) for i in range(len(ndata))))





	

