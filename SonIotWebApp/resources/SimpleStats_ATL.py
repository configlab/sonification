'''
simple stats extraction for the Atlanta Snowstorm twitter data

length: 

location:
{
	lat:{min:, max:, mid:}
	lon:{min:, max:, mid:}
}

timestamp: {min, max, range}

'''

import json
import time
import datetime

fr = open('atlsnow_geo.json', 'r')
fw = open('atlsnow_stats.json', 'w')

lines = fr.readlines()
fr_len = len(lines)
timestamp_list = [None] * fr_len
timestamp_createdAt = [None] * fr_len
lat_list = [None] * fr_len
lon_list = [None] * fr_len

for i, j in enumerate(lines):
	json_obj = json.loads(j)
	lat_list[i] = json_obj['coordinates']['coordinates'][1]
	lon_list[i] = json_obj['coordinates']['coordinates'][0]

	# Convert to Unix Timestamp (taking into account UTC offset)
	timestamp_createdAt[i] = json_obj['created_at']
	timestamp_list[i] = int(time.mktime(datetime.datetime.strptime(json_obj['created_at'],'%a %b %d %H:%M:%S +0000 %Y').timetuple()))
	offset = json_obj['user']['utc_offset']
	if offset != None:
		timestamp_list[i] += int(offset)

	# use millisec, since NYSandy data is doing so...
	timestamp_list[i] *= 1000


lat_min = min(lat_list)
lat_max = max(lat_list)
lon_min = min(lon_list)
lon_max = max(lon_list)
lat_mid = (lat_max - lat_min) / 2 + lat_min
lon_mid = (lon_max - lon_min) / 2 + lon_min

time_min = min(timestamp_list)
time_max = max(timestamp_list)
time_range = time_max - time_min

##################################################

lat_stats = {'min': lat_min, 'max': lat_max, 'mid': lat_mid}
lon_stats = {'min': lon_min, 'max': lon_max, 'mid': lon_mid}
location = {'lat': lat_stats, 'lng': lon_stats}
timestamp = {'min': time_min, 'max': time_max, 'range': time_range}

all_stats = {'length': fr_len, 'location': location, 'timestamp': timestamp}
stats_json = json.dumps(all_stats)
fw.write(stats_json)

fr.close()
fw.close()
