import json

fr = open('nysandy.annotated.json', 'r')
fw = open('categories.txt', 'w')

categories = []

lines = fr.readlines()

for i in lines:
	json_obj = json.loads(i)
	for j in json_obj['categories']:
		categories.append(j)

cat_unique = set(categories)
cat_list = sorted(list(cat_unique))

for cat in cat_list:
	fw.write(cat + '\n')

fr.close()
fw.close()