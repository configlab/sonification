import json
import codecs
import re
from collections import Counter

fr = open('atlsnow_geo_time.json', 'r')
fw = codecs.open('atlsnow_keywords.txt', 'w', 'utf-8')

keywords = []

lines = fr.readlines()

# common words, taken from the internet
# http://en.wikipedia.org/wiki/Most_common_words_in_English
common_words = ['the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'i', 'it', 'for', 'not', 'on', 'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 'or', 'an', 'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which', 'go', 'me', 'when', 'make', 'can', 'like', 'time', 'no', 'just', 'him', 'know', 'take', 'people', 'into', 'year', 'your', 'good', 'some', 'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also', 'back', 'after', 'use', 'two', 'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new', 'want', 'because', 'any', 'these', 'give', 'day', 'most', 'us', 'is', 'are', 'was', 'were']

for i in lines:
	json_obj = json.loads(i)
	text_split = json_obj['text'].split()
	for j in text_split:
		if j.lower() not in common_words:
			keywords.append(j)

# # remove duplicates by converting into a set
# kw_unique = set(keywords)
# kw_list = sorted(list(kw_unique))

kw_ordered = Counter(keywords).most_common()

for kw in kw_ordered:
	# exclude any words containing special & numeric characters
	if re.search('\W+|\d+|_+', kw[0]) is None:
		fw.write(kw[0] + '\n')

fr.close()
fw.close()