#!/usr/bin/env python
#
# cert-stream.py
# Stream the SSL certificates that Shodan is collecting at the moment
#
# WARNING: This script only works with people that have a subscription API plan!
# And by default the Streaming API only returns 1% of the data that Shodan gathers.
# If you wish to have more access please contact us at support@shodan.io for pricing
# information.
#
# Author: achillean

import shodan
import sys

# Configuration
API_KEY = 'cJJ3eMzrXVXbJDK7NGkk4HMtXFZuaZyF'	# Taka's key, need ot change

try:
    # Setup the api
    api = shodan.Shodan(API_KEY)

    print 'Listening for certs...'
    for banner in api.stream.ports([443, 8443]):
        if 'opts' in banner and 'pem' in banner['opts']:
            print banner['opts']['pem']

except Exception, e:
    print 'Error: %s' % e
    sys.exit(1)